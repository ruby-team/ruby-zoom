# -*- coding: UTF-8 -*-
task :default => :test
require 'rake/testtask'
require 'rbconfig'

Rake::TestTask.new do |t|
  # Test files other than package_test.rb require network access (to
  # z3950.loc.gov) (see bug #683153 — Thanks to Felix Geyer for the
  # report and solution!)
  t.test_files = FileList['test/package_test.rb']
  t.ruby_opts << '-rzoom' << '-rtest/unit'
  t.verbose = true
end
